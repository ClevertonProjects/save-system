/*! \struct ConfigData
 *  \brief Has the variables to be saved from the game.
 *  
 *  This struct should be changed accordingly to the project need. The compatible variables type are limited by the Unity JsonUtility's limitation.
 *  int, float, string, array. All these types are tested and worked fine.
 *  
 *  Ex:
 *  public int AttributePoints;                                                         
 *  public int[] SoldierUpgradeLevel;                                         
 *  public float TowerDamage;                                                           
 */
[System.Serializable]
public struct ConfigData
{
    // User Settings.	    
	public float UserZoomSpeed;     					//!< Current user choosen zoom velocity. 
	public float UserRotationRate;  				    //!< Current user choosen rotation velocity.	
	public float UserPanSpeed;  					    //!< Current user choosen pan velocity.
}

using System.IO;
using UnityEngine;

/*! \class Save
 *  \brief Saves and loads the data using an external file. 
 */
public class SaveSystem
{    
    /// <summary>
    /// Check if a save file already exists.
    /// </summary>    
    public bool HasSavedData ()
    {
        return File.Exists(Application.persistentDataPath + "/Save"); 
    }

    /// <summary>
    /// Check if a save file, with a specific name, exists.
    /// </summary>    
    public bool HasSavedData (string fileName)
    {
        return File.Exists(Application.persistentDataPath + "/" + fileName);
    }

    /// <summary>
    /// Loads the saved data.
    /// </summary>
    /// <returns></returns>
    public ConfigData Load ()
    {
        return Load("Save");
    }

    /// <summary>
    /// Loads a saved data file.    
    /// </summary>
    public ConfigData Load (string fileName)
    {        
        string path = Application.persistentDataPath + "/" + fileName;
        string keyPath = Application.persistentDataPath + "/EK";
        ConfigData configData;

        if (File.Exists(path))
        {
            string encryptedContent = File.ReadAllText(path);
            byte[] encryptionKey = File.ReadAllBytes(keyPath);
            string decryptedContent = Encryption.Decrypt(encryptedContent, encryptionKey);

            configData = JsonUtility.FromJson<ConfigData>(decryptedContent);
        }
        else
        {            
            configData.UserZoomSpeed = 1f;
            configData.UserRotationRate = 1f;            
            configData.UserPanSpeed = 1f;
        }

        return configData;
    }

    /// <summary>
    /// Saves the game data.
    /// </summary>    
    public void SaveData (ConfigData configData)
    {
        SaveData(configData, "Save");
    }

    /// <summary>
    /// Saves the game data.
    /// </summary>
    public void SaveData (ConfigData configData, string fileName)
    {
        string path = Application.persistentDataPath + "/" + fileName;
        string keyPath = Application.persistentDataPath + "/EK";

        string saveContent = JsonUtility.ToJson(configData);
        byte[] encryptionKey = Encryption.GenerateKey();       
               
        string encryptedContent = Encryption.Encrypt(saveContent, encryptionKey);
        
        File.WriteAllBytes(keyPath, encryptionKey);
        File.WriteAllText(path, encryptedContent);
	}

	/// <summary>
	/// Erases the game data.
	/// </summary>
	public void Erase ()
    {
        string path = Application.persistentDataPath + "/Save";

        PlayerPrefs.DeleteAll();
        File.Delete(path);
	}
}
